/*
야민정음 후보 문자 추출용 JS 라이브러리
  
# 처리 영역
 * 공역 : 한글 문자 가-힣
 * 치역 : 한글 문자 가-힣
  
 이 외의 문자는 매우 특수한 예외를 제외하고 처리하지않는다.
  
# 데이터
각 한글 문자는 전체 문자의 획수를 계산한다. 자음과 모음에 대하여, 다음과 같이 획수를 정의한다.
  
길이에 상관없이, [종, 횡, 'ㅇ' 문자, 'ㅅ' 문자] 로 저장한다. 획수를 정의하는 기준 문자는 돋움체이다.
  
예를 들어 'ㄱ' 라는 문자는 [1, 1, 0, 0] 으로 정의할 수 있다. 'ㅈ'라는 문자는 [0, 1, 0, 1] 로 정의한다.
  
'ㅎ'는 [0, 2, 1, 0] 으로 정의한다.
  
# 알고리즘
  
1. 입력된 한글 문자의 초,중,종성을 모두 분해하여 획수를 모두 더한다.
1. 이 획수로 만들 수 있는 모든 문자의 경우의 수를 찾는다.
1. 생성된 문자 후보군을 제공한다. 후보군이 없다면 변환하지않는다.
1. 예시로 첫번째 후보만을 선정해 문장을 변환한다.
  
## 경우의 수
  
1. 가장 좋은 후보는 데이터가 일치하는 것이다.  단, 횡값은 -1 0 +1 선에서 오차를 인정한다. (ex. 우미 -> 윽미)
1. 단, 다음 조건에 유의하여야한다.
* 없던 종성이 생기는 건 상관없지만 있던 종성이 사라지는 건 안됨
* 종성이 없는 경우, 중성(모음)이 사라지는 예외가 존재함 (고 -> ㅍ)
1. 조합 방법은 초성 -> 초성 + 중성 -> 초성 + 중성 + 종성 순서대로 조합한다.
  
*/
  
var ymje = function () {
    var res = {
        "strokes": {
            //      종횡ㅇㅅ
            "ㄱ": [1, 1, 0, 0],
            "ㄴ": [1, 1, 0, 0],
            "ㄷ": [1, 2, 0, 0],
            "ㄹ": [2, 3, 0, 0],
            "ㅁ": [2, 2, 0, 0],
            "ㅂ": [2, 2, 0, 0],
            "ㅅ": [0, 0, 0, 1],
            "ㅇ": [0, 0, 1, 0],
            "ㅈ": [0, 1, 1, 0],
            "ㅊ": [0, 2, 0, 1],
            "ㅋ": [1, 2, 0, 0],
            "ㅌ": [3, 1, 0, 0],
            "ㅍ": [2, 2, 0, 0],
             // XXX: 세종머앟 때문에 위의 첫획을 세로로 간주함
            "ㅎ": [1, 1, 1, 0],
            "ㄲ": [2, 1, 0, 0], // F 시계방향 90도
            "ㄸ": [2, 2, 0, 0], // ㅂ 시계방향 90도
            "ㅃ": [3, 2, 0, 0], // ㅂ 에 세로획 하나 추가 
            "ㅆ": [0, 0, 0, 2],
            "ㅉ": [0, 1, 0, 2], // 횡1획 밑에 ㅅ 두 개 그림
            "ㄳ": [1, 1, 0, 1],
            "ㄵ": [1, 2, 0, 1],
            "ㄶ": [1, 3, 1, 0],
            "ㄺ": [3, 3, 0, 0],
            "ㄻ": [4, 3, 0, 0],
            "ㄼ": [4, 3, 0, 0],
            "ㄽ": [2, 3, 0, 1],
            "ㄾ": [3, 3, 0, 0],
            "ㄿ": [4, 3, 0, 0],
            "ㅀ": [2, 5, 1, 0],
            "ㅄ": [2, 2, 0, 1],
            // 모음은 테이블에 작성하지만, 실제로 계산에 사용되기는 어려움.
            // 거의 교체 불가
            "ㅏ": [1, 1, 0, 0],
            "ㅑ": [1, 2, 0, 0],
            "ㅓ": [1, 1, 0, 0],
            "ㅕ": [1, 2, 0, 0],
            "ㅗ": [1, 1, 0, 0],
            "ㅛ": [2, 1, 0, 0],
            "ㅜ": [1, 1, 0, 0],
            "ㅠ": [2, 1, 0, 0],
            "ㅡ": [0, 1, 0, 0],
            "ㅣ": [1, 0, 0, 0],
            "ㅐ": [2, 1, 0, 0],
            "ㅒ": [2, 2, 0, 0],
            "ㅔ": [2, 1, 0, 0],
            "ㅖ": [2, 2, 0, 0],
            "ㅘ": [2, 2, 0, 0],
            "ㅝ": [2, 2, 0, 0],
            "ㅚ": [2, 1, 0, 0],
            "ㅙ": [3, 2, 0, 0],
            "ㅟ": [2, 1, 0, 0],
            "ㅞ": [3, 2, 0, 0],
            "ㅢ": [1, 1, 0, 0]
        },
        "cho": ["ㄱ","ㄲ","ㄴ","ㄷ","ㄸ","ㄹ","ㅁ","ㅂ","ㅃ","ㅅ","ㅆ","ㅇ","ㅈ","ㅉ","ㅊ","ㅋ","ㅌ","ㅍ","ㅎ"],
        "jung": ["ㅏ","ㅐ","ㅑ","ㅒ","ㅓ","ㅔ","ㅕ","ㅖ","ㅗ","ㅘ","ㅙ","ㅚ","ㅛ","ㅜ","ㅝ","ㅞ","ㅟ","ㅠ","ㅡ","ㅢ","ㅣ"],
        "jong": [null, "ㄱ","ㄲ","ㄳ","ㄴ","ㄵ","ㄶ","ㄷ","ㄹ","ㄺ","ㄻ","ㄼ","ㄽ","ㄾ","ㄿ","ㅀ","ㅁ","ㅂ","ㅄ","ㅅ","ㅆ","ㅇ","ㅈ","ㅊ","ㅋ","ㅌ","ㅍ","ㅎ"],
        "ga": "가".charCodeAt(),
        "hih": "힣".charCodeAt()
    }
  
  
    function isDecomposable(c) {
        return c.charCodeAt() >= res.ga && c.charCodeAt() <= res.hih;
    }
  
    function charDecompose(c) {
        var code = c.charCodeAt();
        // 한글 유니코드 베이스
        // 가-힣
        if (!isDecomposable(c))
            return null;
        else {
            // stub
            var decomp = new Array();
            code = code - 0xac00;
            var a = code%28; // 종
            var b = ((code - a)/28) % 21; // 중
            var c = (((code - a)/28) - b) / 21; // 초
            decomp.push(res.cho[c]); // 초성(문자로 변환)
            decomp.push(res.jung[b]); // 중성(문자로 변환)
            decomp.push(res.jong[a]); // 종성(문자로 변환)
            return decomp;
        }
    }

    function charComposeFromDecomp(arr_decomp) {
        var scho = arr_decomp[0] != null ? res.cho.indexOf(arr_decomp[0]) : null;
        var sjung = arr_decomp[1] != null ? res.jung.indexOf(arr_decomp[1]) : null;
        var sjong = arr_decomp[2] != null ? res.jong.indexOf(arr_decomp[2]) : 0;

        // throw new Object()?
        if (scho < 0 || sjung < 0 || sjong < 0) return null;

        if (scho == null) {
            // throw new Object()?
            return null;
        }
        else if (sjung == null) {
            return arr_decomp[0]; // as it is.
        }
        else {
            return String.fromCharCode(0xAC00 + ((scho * 21) + sjung) * 28 + sjong);
        }
    }
  
    function array_atomic4_sum(dst, src) {
        var result = new Array(4);
        result[0] = dst[0] + src[0];
        result[1] = dst[1] + src[1];
        result[2] = dst[2] + src[2];
        result[3] = dst[3] + src[3];
        return result;
    }
  
    function array_atomic4_zerofill(dst) {
        dst[0] = 0;
        dst[1] = 0;
        dst[2] = 0;
        dst[3] = 0;
      
        return dst;
    }
  
    function array_atomic4_eq(a, b) {
        return (a[0] == b[0]) && (a[1] == b[1]) && (a[2] == b[2]) && (a[3] == b[3]);
    }
  
    function array_atomic4_lt(a, b) {
        return (a[0] < b[0]) && (a[1] < b[1]) && (a[2] < b[2]) && (a[3] < b[3]);
    }
  
    function getTotalStrokes(arr_decomp) {
        var strokes = [0, 0, 0, 0];
        for (var c in arr_decomp) {
            var s = res.strokes[arr_decomp[c]];
            if (s) {
                strokes = array_atomic4_sum(strokes, s);
            }
        }
        return strokes;
    }
  
    function greedyCandidates(arr_totalstrokes) {
        /*
        TODO: 위 아래의 획수를 나눠서 계산할 것
        후보군 생성은 기존 문자의 총 획수를 바탕으로, 횡+1 정도 후보를 생성
        */
        var candidates = new Array();
        var goal = arr_totalstrokes;
        var goal_plus = array_atomic4_sum(goal, [0, 1, 0, 0]); // 횡 획을 하나 더
  
        // 초성
        for (var scho in res.cho) {
            scho = res.cho[scho];
            var temp = new Array(4);
            array_atomic4_zerofill(temp);
            temp = array_atomic4_sum(res.strokes[scho], temp);
            if (array_atomic4_eq(temp, goal)) {
                candidates.push([scho, null, null]);
            }
            else {
                // 초성 + 중성
                for (var sjung in res.jung) {
                    sjung = res.jung[sjung];
                    var temp1 = array_atomic4_sum(temp, res.strokes[sjung])
                    if (array_atomic4_eq(temp1, goal) || array_atomic4_eq(temp1, goal_plus)) {
                        candidates.push([scho, sjung, null]);
                    }
                    else {
                        // 초성 + 중성 + 종성
                        for (var sjong in res.jong) {
                            sjong = res.jong[sjong];
                            if (sjong == null) continue;
                            var temp2 = array_atomic4_sum(temp1, res.strokes[sjong]);
                            if (array_atomic4_eq(temp2, goal) || array_atomic4_eq(temp2, goal_plus)) {
                                candidates.push([scho, sjung, sjong]);
                            }
                        }
                    }
                }
            }
        }
        return candidates;
    }
  
    return {
        ymje: function(text) {
            var arr_cand = new Array();
            for (var i in text) {
                var d = charDecompose(text[i]);
                if (typeof d != null) {
                    arr_cand.push(greedyCandidates(getTotalStrokes(d)));
                }
            }

            // recompose into one string
            // by replacing each 'object' in arr_cand
            for (var i in arr_cand) {
                if (typeof arr_cand[i] == 'object') {
                    // join to
                    for (var sub_idx in arr_cand[i]) {
                        arr_cand[i][sub_idx] = charComposeFromDecomp(arr_cand[i][sub_idx]);
                    }

                }
            }

            return arr_cand;

        },
        test_decomp: function (c) {
            console.log(charDecompose(c));
        },
        test_candidates: function (arr_totalstrokes) {
            var t = greedyCandidates(arr_totalstrokes);
            console.log(t);
            console.log(t.length);
        },
        test_comp: function (arr_decomp) {
            console.log(charComposeFromDecomp(arr_decomp));
        }
    }
}();

function show_usage() {
    if (require.main === module) {
        var mod_path = require("path");
        var node_cmd = process.argv[0];
        var cmd = mod_path.basename(process.argv[1]);
        console.log("Usage: " + node_cmd + " " + cmd + " (some korean text)");
        console.log("ex) $ " + node_cmd + " " + cmd + " 세종대왕");
    }
    else {
        console.log("Usage: ymje.ymje([korean chars]) -> returns array of candidates");
    }
}
// Make it run neatly when it's called directly.
if (require.main === module) {
    if (process.argv.length < 3) {
        show_usage();
        process.exit(-1);
    }
    else {
        var text = process.argv[2];
        var arr_cand = ymje.ymje(text);
        var mod_util = require("util");
        for (var char_idx in text) {
            console.log(mod_util.format("[%s:%d] -> %s", text[char_idx], arr_cand[char_idx].length, arr_cand[char_idx].join('')));
        }
    }
}